import {library} from '@fortawesome/fontawesome-svg-core';
import {faSackDollar} from '@fortawesome/free-solid-svg-icons/faSackDollar';
import {faChevronDown} from '@fortawesome/free-solid-svg-icons/faChevronDown';
import {faRotate} from '@fortawesome/free-solid-svg-icons/faRotate';
import {faCheck} from '@fortawesome/free-solid-svg-icons/faCheck';
import {faHandshakeAngle} from '@fortawesome/free-solid-svg-icons/faHandshakeAngle';
import React from 'react';
import {Provider} from 'react-redux';
import Navigator from './navigator';
import {store} from './src/redux';

library.add(faSackDollar, faChevronDown, faRotate, faCheck, faHandshakeAngle);
const Index = () => {
    return (
        <Provider store={store}>
            <Navigator />
        </Provider>
    );
};

export default Index;
