import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import _, {COLOR} from './src/styles';
import {StyleSheet, Appearance} from 'react-native';
import {patchTheme} from './src/redux/actions/theme';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createNativeStackNavigator();

const components = {
    splashScreen: {
        SplashScreen: require('./src/screens/Splashscreen'),
    },
    app: {
        Home: require('./src/screens/index'),
        TransactionSuccess: require('./src/screens/TransactionSuccess'),
    },
};

const initialTheme = ({isDarkMode}) => {
    const getColor = (dark, light) => {
        return isDarkMode ? COLOR[dark] || dark : COLOR[light] || light;
    };

    const colors = {
        ...COLOR,
        ...{
            statusBar: getColor('light-content', 'dark-content'), // untuk StatusBar Style
            border: getColor('white5', '#d5e0e8'), // untuk border color
            background: getColor('black', 'white2'), // untuk latar belakang
            container: getColor('black4', 'white'), // untuk kontainer  cth: card, view berisi teks, image
            container2: getColor('black5', 'white'), // untuk kontainer  cth: card, view berisi teks, image
            text: getColor('white', 'black'), // teks biasa
            text_alt: getColor('white3', 'white5'), // teks abu abu
            text_white: getColor('black', 'white'), // teks biasa
            grey: getColor('#666666', '#e5e5e5'), // untuk garis pemisah, check tidak akttif dll
        },
    };

    const styles = StyleSheet.create({
        // untuk latar belakang
        background: {
            backgroundColor: colors.background,
        },
        bgGrey: {
            backgroundColor: colors.grey,
        },
        // untuk kontainer  cth: card, view berisi teks, image
        container: {
            backgroundColor: colors.container,
        },
        // untuk kontainer yg parentnya sudah container juga
        container2: {
            backgroundColor: colors.container2,
        },
    });

    styles.card = {
        borderRadius: 12,
        overflow: 'hidden',
        borderWidth: isDarkMode ? 0 : 1,
        borderColor: '#eee',
        ...styles.container,
        ..._.p,
    };

    styles.card2 = {
        ...styles.card,
        ...styles.container2,
    };

    return {
        isDarkMode,
        colors,
        styles,
    };
};

const Navigator = () => {
    const {isDarkMode = false} = useSelector(s => s.theme);
    const {loading} = useSelector(s => s.auth);

    const dispatch = useDispatch();

    const componentsKey = loading ? 'splashScreen' : 'app';
    const initialRoute = loading ? 'SplashScreen' : 'Home';
    const selectedComponents = components[componentsKey];
    const theme = initialTheme({
        isDarkMode,
    });

    useEffect(() => {
        const handleListenerApperance = ({colorScheme}) => {
            dispatch(patchTheme({isDarkMode: colorScheme === 'dark'}));
        };
        Appearance.addChangeListener(handleListenerApperance);

        return () => {
            Appearance.removeChangeListener(handleListenerApperance);
        };
    }, [dispatch]);
    return (
        <NavigationContainer theme={theme}>
            <Stack.Navigator initialRouteName={initialRoute} screenOptions={{headerShown: false}}>
                {Object.keys(selectedComponents).map((x, i) => {
                    const component = selectedComponents[x].default;
                    return <Stack.Screen key={i} name={x} component={component} />;
                })}
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default Navigator;
