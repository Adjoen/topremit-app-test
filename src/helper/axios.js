import axios from 'axios';
import {Dimensions, Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

const createAxios = async baseURL => {
    const paramsAxios = {
        os: Platform.OS,

        mobile_version: DeviceInfo.getVersion(),
        device_hardware: await DeviceInfo.getHardware(),
        device_manufacturer: await DeviceInfo.getManufacturer(),
        device_type: await DeviceInfo.getDevice(),
        device_os_name: DeviceInfo.getSystemName(),
        device_os_type: DeviceInfo.getSystemVersion(),

        device_width: Dimensions.get('window').width,
        device_height: Dimensions.get('window').height,
    };

    var configAxios = {
        baseURL,
        timeout: 15000,
        headers: {
            Accept: 'application/json',
        },
        params: paramsAxios,
    };

    return axios.create(configAxios);
};

const handleError = (error, reject, logoutUnAuth = true) => {
    const {response} = error;
    const status = response?.status;

    let error_title = '';
    if (response?.data?.title) {
        error_title = response?.data?.title;
    } else if (response?.data?.message) {
        error_title = response?.data?.message;
    } else {
        error_title = error.message;
    }

    switch (status) {
        case 401:
            let error_title401 = '';
            if (!logoutUnAuth) {
                error_title401 += 'Akses tidak diizinkan.\n';
            } else {
                error_title401 += 'Akses login tidak valid, Silahkan coba login kembali.\n';
            }
            error_title401 += error_title;
            reject(new CustomError({message: error_title401, code: 401}));
            break;
        case 404:
            reject(
                new CustomError({
                    message: 'Code 404: Akses yang anda minta tidak ditemukan, silahkan coba akses ke halaman lain.\n' + error_title,
                    code: 404,
                })
            );
            break;
        case 500:
            reject(new CustomError({message: 'Code 500: Telah Terjadi Kesalahan Internal Server.\n' + error_title, code: 500}));
            break;

        default:
            if (response === undefined) {
                if (error.message?.includes('timeout')) {
                    reject(new Error('Waktu akses habis, silahkan coba kembali.\n' + error_title));
                } else if (error.message?.includes('Network Error')) {
                    reject(new Error('Tidak dapat terhubung, silahkan coba kembali.\n' + error_title));
                } else {
                    reject(new Error(error.message));
                }
            } else {
                reject(new ValidationError({message: error_title, data: response.data?.data}));
            }
            break;
    }
};

export const GET = (uri, params = {}) => {
    let logoutUnAuth = true;

    return new Promise(async (resolve, reject) => {
        const api = await createAxios();

        api.get(uri, {params})
            .then(resolve)
            .catch(err => {
                handleError(err, reject, logoutUnAuth);
            });
    });
};

export const POST = (uri, params = {}) => {
    let logoutUnAuth = true;

    return new Promise(async (resolve, reject) => {
        const api = await createAxios();

        api.post(uri, {params})
            .then(resolve)
            .catch(err => {
                handleError(err, reject, logoutUnAuth);
            });
    });
};

export class ValidationError extends Error {
    constructor(e) {
        super(e);
        this.data = e.data;
        this.message = e.message;
    }
}
export class CustomError extends Error {
    constructor(e) {
        super(e);
        this.code = e.code;
        this.name = e.name ?? 'ErrorHttpRequest';
        this.message = e.message;
    }
}
