const formatCurrency = (str = '0') => {
    if (typeof str === 'number' && str > 0) {
        str = str.toString();
    }
    return str.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,');
};
export default formatCurrency;
