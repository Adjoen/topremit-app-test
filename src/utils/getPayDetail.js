const getPayDetail = (amount, get_receipt = true) =>
    new Promise((resolve, reject) =>
        setTimeout(() => {
            const rates = Math.floor(Math.random() * 5000);
            const fees = Math.floor(Math.random() * 50000);
            const receipt_amount = (amount / rates).toFixed(2).toString();

            if (receipt_amount < 1 && get_receipt) {
                reject({message: `transaction must be above ${rates} IDR`});
            } else {
                resolve({data: {rates, fees, receipt_amount}});
            }
        }, 3000)
    );

export default getPayDetail;
