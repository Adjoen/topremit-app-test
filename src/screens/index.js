import {useTheme} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {Card, FokusAwareStatusBar, Text} from '../components';
import _ from '../styles';
import {isEmpty} from '../utils';
import {Dropdown, PaymentMethod, PayDetail} from './_components.js';

const Index = ({navigation}) => {
    const {colors} = useTheme();
    const [country, setCountry] = useState({});
    const [method, setMethod] = useState();

    useEffect(() => {
        if (!isEmpty(country)) {
            setMethod('bank');
        }
    }, [country]);

    const submit = () => {
        setCountry({});
        setMethod();
        navigation.push('TransactionSuccess');
    };

    return (
        <>
            <FokusAwareStatusBar translucent={false} backgroundColor={colors.primary} barStyle="light-content" />
            <View style={_.flex}>
                <ScrollView contentContainerStyle={_.flexGrow}>
                    <View style={[_.p, _.pt_5, _.flex]}>
                        <Text color="primaryLight" weight="bold" size={50} style={[{letterSpacing: -1, lineHeight: 50}, _.mb]}>
                            SEND{'\n'}MONEY
                        </Text>
                        <Card>
                            <Dropdown value={country} setValue={setCountry} />

                            {!isEmpty(country) && <PaymentMethod value={method} setValue={setMethod} />}

                            {!isEmpty(country) && !isEmpty(method) && (
                                <PayDetail method={method} country={country} currency={Object.keys(country?.currencies)[0]} submit={submit} />
                            )}
                        </Card>
                    </View>
                </ScrollView>
            </View>
        </>
    );
};

export default Index;
