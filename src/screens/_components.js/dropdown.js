import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {useIsFocused, useTheme} from '@react-navigation/native';
import React, {useRef, useState, useEffect} from 'react';
import {FlatList, TouchableOpacity, View, ActivityIndicator} from 'react-native';
import {Text, ModalBottom, Image} from '../../components';
import {GET} from '../../helper/axios';
import _ from '../../styles';
import {isEmpty} from '../../utils';

/**
 *
 * @typedef Props
 * @property {any} value
 * @property {Function} setValue
 */

const CustomDropDownPicker = (
    /**
     * @type Props
     */
    {value = {}, setValue = () => {}}
) => {
    const isFocus = useIsFocused();
    const modal = useRef();
    const flatlistRef = useRef();
    const {colors} = useTheme();

    const [label, setLabel] = useState('');
    const [loading, setLoading] = useState(true);
    const [options, setOptions] = useState([]);
    const [error, setError] = useState(false);
    const [errMessage, setErrMessage] = useState('');

    const placeHolder = 'Choose Country';

    //#region Method

    const changeValue = v => {
        setValue(v);
        setLabel(v.name.common);
        modal.current?.close();
    };

    const renderItems = ({item, index}) => {
        const isChoosed = value?.cca2 == item.cca2;
        return (
            <TouchableOpacity
                onPress={() => changeValue(item)}
                style={[
                    _.p_2,
                    _.pv_4,
                    _.row,
                    _.itemsCenter,
                    _['border-b'],
                    {
                        backgroundColor: colors[isChoosed ? 'primaryLight' : 'transparent'],

                        borderBottomColor: colors.border,
                    },
                ]}>
                <Image source={{uri: item.flags.png}} width={30} style={_.mr_1} />
                <Text color={colors[isChoosed ? 'white' : 'text2']} style={_.flex}>
                    {item.name.common ?? '-'}
                </Text>
                <FontAwesomeIcon icon="check" color={colors.white} />
            </TouchableOpacity>
        );
    };

    useEffect(() => {
        if (isEmpty(value)) {
            setLabel('');
        }
    }, [value]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        setLoading(true);
        try {
            const res = await GET('https://restcountries.com/v3.1/all');

            if (res.data.length > 0) {
                setOptions(res.data);
            } else {
                throw new Error(res.data.message);
            }
        } catch (err) {
            setError(true);
            setErrMessage(err.message);
        }
        setLoading(false);
    };

    const doRefresh = () => {
        setError(false);
        setOptions([]);
        fetchData();
    };

    return (
        isFocus && (
            <>
                <ModalBottom ref={modal} disabelScrollView>
                    <FlatList
                        ref={flatlistRef}
                        data={options?.length > 0 ? options : []}
                        renderItem={renderItems}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={item => item?.cca2}
                        ListHeaderComponent={
                            <Text color={colors.text} align="center" style={[_.mv_2]}>
                                {placeHolder}
                            </Text>
                        }
                        onScrollToIndexFailed={info => {
                            try {
                                const wait = new Promise(resolve => setTimeout(resolve, 10));
                                wait.then(() => {
                                    flatlistRef?.current?.scrollToIndex({
                                        animated: true,
                                        index: info.index,
                                    });
                                });
                            } catch (err) {}
                        }}
                        ListFooterComponent={
                            !Array.isArray(options) ||
                            (Array.isArray(options) && options.length < 1 && (
                                <View style={(_.mvAuto, {height: 120})}>
                                    <Text align="center" color={colors.text} style={[_.mvAuto]}>
                                        Opsi tidak tersedia
                                    </Text>
                                </View>
                            ))
                        }
                        style={[_.pb_5]}
                    />
                </ModalBottom>
                <TouchableOpacity
                    onPress={() => {
                        if (error) {
                            doRefresh();
                        } else if (!loading) {
                            modal.current.show();
                        }
                    }}
                    style={[_.row, _.itemsCenter, _.p_5, _.border, _['rounded-sm'], {borderColor: colors.border}]}>
                    {value?.flags?.png !== undefined && <Image source={{uri: value.flags.png}} width={30} style={_.mr_1} />}
                    <Text numberOfLines={1} color={colors.text} size={14} style={[_.flex]}>
                        {label || placeHolder}
                    </Text>
                    {!error ? (
                        !loading ? (
                            <FontAwesomeIcon icon="chevron-down" size={14} />
                        ) : (
                            <ActivityIndicator color={colors.primary} />
                        )
                    ) : (
                        <FontAwesomeIcon icon="rotate" color={colors.danger} />
                    )}
                </TouchableOpacity>
                {error && (
                    <Text color="danger" size="sm" style={_.p}>
                        {errMessage}
                    </Text>
                )}
            </>
        )
    );
};
export default CustomDropDownPicker;
