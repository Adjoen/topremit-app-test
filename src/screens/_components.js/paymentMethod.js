import React from 'react';
import {Button} from '../../components';
import {View} from 'react-native';
import _ from '../../styles';
const Index = ({value, setValue}) => {
    const methods = [
        {
            label: 'Bank Transfer',
            value: 'bank',
        },
        {
            label: 'Instant Transfer',
            value: 'instant',
        },
        {
            label: 'Cash Pickup',
            value: 'cash',
        },
    ];

    return (
        <View style={[_.row, _.flexWrap, _.mv]}>
            {methods.map((x, i) => {
                const isActive = x.value === value;
                return (
                    <View key={i} style={_.p_2}>
                        <Button onPress={() => setValue(x.value)} style={_.ph_3} outline={!isActive} color="primaryLight">
                            {x.label}
                        </Button>
                    </View>
                );
            })}
        </View>
    );
};

export default Index;
