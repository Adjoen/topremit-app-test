import {useTheme} from '@react-navigation/native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {TextInput, View, ActivityIndicator} from 'react-native';
import {Button, Text} from '../../components';
import _, {FONT} from '../../styles';
import {getPayDetail, isEmpty} from '../../utils';
import formatCurrency from '../../utils/formatCurrency';

const Index = ({method, country, currency, submit}) => {
    const defaultAmount = '1000000';
    const {colors} = useTheme();
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState({});
    const [error, setError] = useState(false);
    const [errMessage, setErrMessage] = useState('');
    const [amountSend, setAmountSend] = useState(defaultAmount);
    const [amountReceipt, setAmountReceipt] = useState('0');

    const amountTimeOutRef = useRef();

    useEffect(() => {
        fetchData();
    }, [method, country, fetchData]);

    const fetchData = useCallback(
        async (value = amountSend, send = true) => {
            setLoading(true);
            setError(false);
            try {
                const res = await getPayDetail(value, send);
                const result = res.data;
                if (!isEmpty(result)) {
                    setData(result);
                    if (send) {
                        setAmountReceipt(result.receipt_amount);
                    } else {
                        setAmountSend((value * result.rates).toFixed(2).toString());
                    }
                } else {
                    throw new Error(result);
                }
            } catch (err) {
                console.log(err);
                setErrMessage(err.message);
                setError(true);
            }
            setLoading(false);
        },
        [amountSend]
    );

    return (
        <View>
            <AmountInput
                loading={loading}
                value={amountSend}
                setValue={e => {
                    setAmountSend(e);
                    clearTimeout(amountTimeOutRef.current);
                    amountTimeOutRef.current = setTimeout(() => {
                        fetchData(e);
                    }, 3000);
                }}
                currency="IDR"
                label="You send"
            />

            {!error ? (
                !loading ? (
                    <>
                        <View style={_.p}>
                            <View style={[_.row, _.itemsCenter, _.mb]}>
                                <Text weight="bold" style={_.flex}>
                                    Rates
                                </Text>
                                <Text align="right">{formatCurrency(data.rates)}</Text>
                            </View>
                            <View style={[_.row, _.itemsCenter, _.mb]}>
                                <Text weight="bold" style={_.flex}>
                                    Fees
                                </Text>
                                <Text align="right">{formatCurrency(data.fees)}</Text>
                            </View>
                        </View>
                        <AmountInput
                            loading={loading}
                            value={amountReceipt}
                            setValue={e => {
                                setAmountReceipt(e);
                                clearTimeout(amountTimeOutRef.current);
                                amountTimeOutRef.current = setTimeout(() => {
                                    fetchData(e, false);
                                }, 3000);
                            }}
                            currency={currency}
                            label="Receipent get"
                        />
                        <Button rounded onPress={submit}>
                            Continue
                        </Button>
                    </>
                ) : (
                    <View>
                        <ActivityIndicator color={colors.primary} size="large" />
                    </View>
                )
            ) : (
                <View style={_.p}>
                    <Text align="center" size="md" color="danger">
                        {errMessage}
                    </Text>
                </View>
            )}
        </View>
    );
};

const AmountInput = ({value, setValue, currency, label = '', loading}) => {
    const {colors} = useTheme();
    return (
        <View style={[_['rounded-sm'], _.row, _.overflowHidden, _.border, {borderColor: colors.border}, _.mb]}>
            <View style={[_.flex, _.ph]}>
                <Text size="xs" color="text_alt" style={_.mt_1}>
                    {label}
                </Text>
                <TextInput
                    editable={!loading}
                    placeholder="0"
                    style={[{fontFamily: FONT.regular, fontSize: 18, marginTop: -4}]}
                    value={value}
                    onChangeText={setValue}
                />
            </View>
            <View style={[_.ph, _.mv_1, _['border-l'], {borderColor: colors.border}]}>
                <Text style={[{letterSpacing: 2}, _.mvAuto]}>{currency}</Text>
            </View>
        </View>
    );
};

export default Index;
