import {View, ActivityIndicator} from 'react-native';
import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {passLoading} from '../../redux/actions/auth';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {Text, FokusAwareStatusBar} from '../../components';
import _ from '../../styles';
import {useTheme} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';

const Index = () => {
    const {colors} = useTheme();
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            dispatch(passLoading());
        }, 5000);
    }, [dispatch]);

    return (
        <>
            <FokusAwareStatusBar barStyle="light-content" />
            <LinearGradient style={[_.flex, _.p]} colors={['#74ebd5', '#ACB6E5']}>
                <View style={_.mvAuto}>
                    <View style={[_['rounded-full'], _.mb, _.mhAuto, _.bgWhite, _.overflowHidden, {height: 150, width: 150, borderRadius: 150}]}>
                        <FontAwesomeIcon icon="sack-dollar" style={[_.mAuto]} size={80} color="#006E7F" />
                    </View>

                    <View style={[_.row, _.itemsCenter, _.contentCenter]}>
                        <ActivityIndicator color={colors.primary} />
                        <Text color="black" style={_.ml_1}>
                            Loading...
                        </Text>
                    </View>
                </View>
            </LinearGradient>
        </>
    );
};

export default Index;
