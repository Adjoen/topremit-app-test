import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View} from 'react-native';
import {Button, Card, FokusAwareStatusBar, Text} from '../../components';
import _ from '../../styles';

const Index = ({navigation}) => {
    const {colors, styles} = useTheme();

    return (
        <>
            <FokusAwareStatusBar translucent={false} backgroundColor={colors.primary} barStyle="light-content" />
            <View style={[_.flex, _.p, styles.background]}>
                <Card style={_.flex}>
                    <View style={_.mvAuto}>
                        <Text align="center">
                            <FontAwesomeIcon size={200} icon="handshake-angle" color="#E8BEAC" />
                        </Text>
                        <Text size={30} align="center" style={_.mb}>
                            Transaction success
                        </Text>
                        <Button rounded onPress={navigation.pop}>
                            Back
                        </Button>
                    </View>
                </Card>
            </View>
        </>
    );
};

export default Index;
