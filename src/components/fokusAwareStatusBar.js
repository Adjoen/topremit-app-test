import * as React from 'react';
import {StatusBar} from 'react-native';
import {useIsFocused, useTheme} from '@react-navigation/native';

/**
 * @param {import('react-native').StatusBarProps} props
 */
const FokusAwareStatusBar = ({translucent = true, backgroundColor = 'transparent', barStyle}) => {
    const isFocused = useIsFocused();
    const {colors, isDarkMode} = useTheme();

    return isFocused ? (
        <StatusBar
            translucent={translucent}
            backgroundColor={colors[backgroundColor] || backgroundColor}
            barStyle={barStyle ? barStyle : isDarkMode ? 'light-content' : 'dark-content'}
        />
    ) : null;
};
export default FokusAwareStatusBar;
