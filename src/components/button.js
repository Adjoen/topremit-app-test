import {useTheme} from '@react-navigation/native';
import PropTypes from 'prop-types';
import React from 'react';
import {ActivityIndicator, Text as _T, TouchableOpacity, ViewPropTypes} from 'react-native';
import {isEmpty} from '../utils';
import {Text} from '.';

/**
 * Component
 * @augments {Component<Props, State>}
 *
 *
 * example:
 *  <Button>
 *      SELANJUTNYA
 *  </Button>
 *
 *
 */

const Button = ({
    children,
    numberOfLines,
    expand,
    style,
    styleText,
    onPress,
    loading,
    color,
    disable,
    rounded,
    outline,
    outlineWithBackground,
    small,
    colorOutLine,
    textColor,
}) => {
    const {colors} = useTheme();
    textColor = !isEmpty(textColor) ? textColor : outline ? (!isEmpty(color) ? color : 'primary') : colors.white;
    return (
        <TouchableOpacity
            disabled={disable}
            activeOpacity={loading ? 1 : 0.7}
            onPress={() => {
                if (!loading && !disable) {
                    onPress();
                }
            }}
            style={[
                {
                    borderRadius: rounded ? 100 : 4,
                    minHeight: !small ? 45 : 35,
                    padding: 4,
                    paddingHorizontal: !expand ? 8 : 4,
                    borderWidth: outline ? 1 : 0,
                    borderColor: !outline ? undefined : disable ? colors.silver : colors[color] ? colors[color] : color,
                    backgroundColor: outline
                        ? !outlineWithBackground
                            ? 'transparent'
                            : disable
                            ? colors.silver
                            : colors[colorOutLine]
                            ? colors[colorOutLine]
                            : colorOutLine
                        : colors[color]
                        ? disable
                            ? colors.silver
                            : colors[color]
                        : disable
                        ? colors.silver
                        : color,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: !expand ? 'center' : undefined,
                },
                style,
            ]}>
            {loading ? (
                <ActivityIndicator color={!outline ? colors.white : colors[textColor] ? colors[textColor] : textColor} />
            ) : (
                <>
                    {typeof children == 'string' || (typeof children == 'object' && children.length) ? (
                        <Text
                            style={[{textAlign: 'center', marginTop: -2}, styleText]}
                            numberOfLines={numberOfLines}
                            size={!small ? 16 : 12}
                            color={
                                !outline || outlineWithBackground
                                    ? textColor
                                    : disable
                                    ? colors.silver
                                    : colors[textColor]
                                    ? colors[textColor]
                                    : textColor
                            }>
                            {children}
                        </Text>
                    ) : (
                        children
                    )}
                </>
            )}
        </TouchableOpacity>
    );
};

Button.propTypes = {
    numberOfLines: PropTypes.any,
    children: PropTypes.any,
    expand: PropTypes.bool,
    style: ViewPropTypes.style,
    styleText: _T.propTypes.style,
    onPress: PropTypes.func,
    loading: PropTypes.bool,
    color: PropTypes.string,
    disable: PropTypes.bool,
    icon: PropTypes.any,
    textColor: PropTypes.string,
    rounded: PropTypes.bool,
    outline: PropTypes.bool,
    small: PropTypes.bool,
    outlineWithBackground: PropTypes.bool,
    colorOutLine: PropTypes.string,
};

Button.defaultProps = {
    numberOfLines: 3,
    expand: true,
    onPress: () => console.warn('ButtonComponent: OnPress Kosong'),
    styleText: {},
    color: 'primary', // 'primary', 'secondary'
    small: false,
    loading: false,
    textColor: '',
    disable: false,
    rounded: false,
    outline: false,
    outlineWithBackground: false,
    colorOutLine: '',
};

export default Button;
