import {useTheme, useFocusEffect} from '@react-navigation/native';
import PropTypes from 'prop-types';
import React, {forwardRef, useEffect, useImperativeHandle, useRef, useState} from 'react';
import {
    Animated,
    Easing,
    Keyboard,
    Modal,
    PanResponder,
    SafeAreaView,
    ScrollView,
    TouchableWithoutFeedback,
    View,
    Dimensions,
    ViewPropTypes,
    StatusBar,
} from 'react-native';
import _ from '../styles';

/**
 * @typedef Props
 * @property {import('react-native').ViewStyle} style
 * @property {*} children
 * @property {Boolean} closeable
 * @property {Boolean} allowOverlayClose
 * @property {Array} supportedOrientations
 * @property {Any} refreshControl
 * @property {Boolean} disabelScrollView
 * @property {Function} onClose
 *
 */
const ModalBottom = forwardRef(
    (
        /**
         * @type Props
         */
        {
            style,
            children,
            closeable = true,
            allowOverlayClose,
            supportedOrientations,
            onClose,
            height = undefined,
            refreshControl,
            disabelScrollView = false,
        },
        _ref
    ) => {
        const [visible, setVisible] = useState(false),
            [focused, setFocused] = useState(false),
            {styles, colors} = useTheme(),
            keyboardShow = useRef(false);
        const statusBarHeight = StatusBar.currentHeight;
        const screenHeight = Dimensions.get('window').height;
        const bottom = React.useRef(new Animated.Value(-(height ?? screenHeight) / 2)).current;
        const StatusBarHeight = statusBarHeight < 20 ? statusBarHeight + 12 : 20;
        const screenHeightRef = React.useRef(height ?? screenHeight);

        useFocusEffect(
            React.useCallback(() => {
                setFocused(true);
                return () => {
                    setFocused(false);
                };
            }, [])
        );

        useEffect(() => {
            const _keyboardDidShow = e => {
                keyboardShow.current = true;
                Animated.timing(bottom, {
                    toValue: e.endCoordinates.height,
                    duration: 250,
                    useNativeDriver: false,
                    easing: Easing.ease,
                }).start();
            };
            const _keyboardDidHide = e => {
                keyboardShow.current = false;
                Animated.timing(bottom, {
                    toValue: 0,
                    duration: 250,
                    useNativeDriver: false,
                    easing: Easing.ease,
                }).start();
            };
            const listenerShow = Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
            const listenerHide = Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

            // cleanup function
            return () => {
                listenerShow.remove();
                listenerHide.remove();
            };
        }, [bottom]);

        useEffect(() => {
            if (screenHeightRef.current != screenHeight && height == undefined) {
                screenHeightRef.current = screenHeight;
            }
        }, [height, screenHeight]);

        useEffect(() => {
            if (height != undefined) {
                screenHeightRef.current = height;
            }
        }, [height]);

        useEffect(() => {
            if (visible) {
                Animated.timing(bottom, {
                    toValue: 0,
                    duration: 250,
                    useNativeDriver: false,
                    easing: Easing.ease,
                }).start();
            }
        }, [bottom, visible]);

        useImperativeHandle(_ref, () => ({
            show() {
                setVisible(true);
            },
            close,
            isShowed() {
                return visible;
            },
        }));

        const close = (force = false) => {
            if (force) {
                processClose(force);
            } else {
                Animated.timing(bottom, {
                    toValue: -(height ?? screenHeight),
                    duration: 250,
                    useNativeDriver: false,
                    easing: Easing.ease,
                }).start(() => {
                    processClose(force);
                });
            }
        };

        const processClose = (force = false) => {
            setVisible(false);
            if (typeof onClose === 'function') {
                onClose(force);
            }
        };

        const panResponder = React.useRef(
            PanResponder.create({
                onStartShouldSetPanResponder: (evt, gestureState) => true,
                onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
                onMoveShouldSetPanResponder: (evt, gestureState) => true,
                onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
                onPanResponderGrant: (evt, gestureState) => {},
                onPanResponderMove: (evt, {moveY, y0}) => {
                    if (y0 - moveY <= 0 && !keyboardShow.current) {
                        bottom.setValue(y0 - moveY);
                    }
                },
                onPanResponderTerminationRequest: (evt, gestureState) => true,
                onPanResponderRelease: (evt, gestureState) => {
                    if (!keyboardShow.current) {
                        let heightContent = screenHeightRef.current;
                        const {moveY, y0} = gestureState;
                        const treshold = y0 + (heightContent - y0) / 2; // Membuat Treshold untuk pengecekan pemindahan gesture, Measuring 1/2 dari Content
                        if (moveY > treshold) {
                            close(true);
                        } else {
                            Animated.timing(bottom, {
                                toValue: 0,
                                duration: 250,
                                useNativeDriver: false,
                                easing: Easing.ease,
                            }).start();
                        }
                    }
                },
                onPanResponderTerminate: (evt, gestureState) => {},
                onShouldBlockNativeResponder: (evt, gestureState) => {
                    return true;
                },
            })
        ).current;

        return !focused ? null : (
            <Modal
                supportedOrientations={supportedOrientations}
                transparent
                statusBarTranslucent={true}
                visible={visible}
                onRequestClose={() => {
                    if (closeable) {
                        close();
                    }
                }}
                animationType="none">
                <TouchableWithoutFeedback
                    onPress={() => {
                        if (closeable && allowOverlayClose) {
                            close();
                        }
                    }}>
                    <View style={[{backgroundColor: '#00000050', zIndex: 1}, _.overlay]} />
                </TouchableWithoutFeedback>
                <Animated.View
                    style={[
                        {
                            bottom,
                            maxHeight: (height ?? screenHeight) - StatusBarHeight * 4,
                            position: 'absolute',
                            right: 0,
                            left: 0,
                            zIndex: 2,
                        },
                    ]}>
                    {closeable === true && (
                        <View
                            style={[
                                {
                                    position: 'absolute',
                                    top: -50,
                                    left: 0,
                                    right: 0,
                                    paddingTop: 65,
                                    paddingBottom: 10,
                                    zIndex: 100,
                                },
                                _.itemsCenter,
                                _.contentCenter,
                            ]}
                            {...panResponder.panHandlers}>
                            <View style={[{height: 10, width: 60, backgroundColor: colors.text_alt}, _['rounded-full']]} />
                        </View>
                    )}
                    <SafeAreaView style={{backgroundColor: colors.container, borderTopLeftRadius: 12, borderTopRightRadius: 12}}>
                        <View
                            style={[
                                styles.container,
                                {borderTopLeftRadius: 12, borderTopRightRadius: 12, overflow: 'hidden'},
                                style,
                                _.ptm(closeable ? 30 : 16),
                            ]}>
                            {React.createElement(
                                disabelScrollView ? View : ScrollView,
                                {refreshControl, style: [_.ph], contentContainerStyle: {paddingBottom: 10}, keyboardShouldPersistTaps: 'handled'},
                                <View>{children}</View>
                            )}
                        </View>
                    </SafeAreaView>
                </Animated.View>
            </Modal>
        );
    }
);

ModalBottom.propTypes = {
    style: ViewPropTypes.style,
    onRequestClose: PropTypes.func,
    closeable: PropTypes.bool,
    allowOverlayClose: PropTypes.bool,
    supportedOrientations: PropTypes.array,
};

ModalBottom.defaultProps = {
    closeable: true,
    allowOverlayClose: true,
    supportedOrientations: ['portrait'],
};

export default ModalBottom;
