import {useTheme} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Text} from 'react-native';
import {FONT, FONT_SIZE} from '../styles';

/**
 *
 * @typedef Props
 * @property {String} color
 * @property {("bold" | "boldItalic" | "extraBold" | "extraBoldItalic" | "italic" | "light" | "lightItalic" | "medium" | "mediumItalic" | "regular" | "semiBold" | "semiBoldItalic" )} weight
 * @property {("xs" | "sm" | "md" | "lg" | "xl" | "2xl" | "3xl" | "4xl" | "5xl")} size
 * @property {("left" | "center" | "right")} align
 * @property {Number} numberOfLines
 * @property {("head" | "middle" | "tail" | "clip")} ellipsizeMode
 * @property {import('react-native').TextStyle} style
 */

const CustomText = (
    /**
     * @type Props
     */
    {color = 'black', weight = 'medium', size = 'md', align = 'left', style, onPress, children, numberOfLines, ellipsizeMode}
) => {
    const [isMounted, setIsMounted] = React.useState(false);
    const {colors} = useTheme();
    useEffect(() => {
        setIsMounted(true);

        return () => {
            setIsMounted(false);
        };
    }, []);

    const styles = {
        color: colors[color] ? colors[color] : typeof color === 'string' ? color : colors.black,
        fontFamily: FONT[weight],
        fontSize: FONT_SIZE?.[size] ? FONT_SIZE[size] : typeof size === 'string' ? parseInt(size, 10) : size,
        textAlign: align,
    };

    return !isMounted ? null : (
        <Text onPress={onPress} style={[styles, style]} numberOfLines={numberOfLines} ellipsizeMode={ellipsizeMode}>
            {children}
        </Text>
    );
};
export default CustomText;
