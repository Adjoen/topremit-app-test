import React, {useEffect} from 'react';
import {Image as IMG} from 'react-native';

/**
 * @param {Object} default
 * @param {("cover" | "contain" | "stretch" | "repeat" | "center")} [default.resizeMode = "cover"]
 * @param {} default.source
 * @param {Number} [default.height = 0]
 * @param {Number} [default.width = 0]
 * @param {Function} [default.callbackHeight] - return Number (0)
 * @param {StyleProp<ImageStyle>} [default.style]
 */

const Image = ({resizeMode = 'cover', source, style, height = 0, width = 0, callbackHeight}) => {
    var [imgHeight, setHeight] = React.useState(0),
        [imgWidth, setWidth] = React.useState(0),
        [mounted, setMounted] = React.useState(false);

    useEffect(() => {
        imgHeight = height;
        imgWidth = width;
        setHeight(height);
        setWidth(width);
        setMounted(true);
        if (!(height && width)) {
            setSize();
        }
        return () => {
            setMounted(false);
        };
    }, [height, width, source?.uri]);

    const setSize = async () => {
        if (typeof source !== 'object') {
            const newHeight = await IMG.resolveAssetSource(source).height;
            const newWidth = await IMG.resolveAssetSource(source).width;
            if (imgWidth && !imgHeight) {
                setHeight(newHeight * (imgWidth / newWidth));
                if (callbackHeight) {
                    callbackHeight(newHeight * (imgWidth / newWidth));
                }
            } else if (!imgWidth && imgHeight) {
                setWidth(newWidth * (imgHeight / newHeight));
            }
        } else if (typeof source === 'object' && source.uri) {
            const headers = source.headers;
            const uri = source.uri;
            await IMG.getSizeWithHeaders(uri, headers, (newWidth, newHeight) => {
                if (imgWidth && !imgHeight) {
                    setHeight(newHeight * (imgWidth / newWidth));
                    if (callbackHeight) {
                        callbackHeight(newHeight * (imgWidth / newWidth));
                    }
                } else if (!imgWidth && imgHeight) {
                    setWidth(newWidth * (imgHeight / newHeight));
                }
            });
        }
    };

    return (typeof source !== 'object' || source?.uri !== '') && mounted === true ? (
        <IMG
            resizeMode={resizeMode}
            source={source}
            style={[
                {
                    height: !isNaN(imgHeight) ? imgHeight : 10,
                    width: imgWidth,
                    marginRight: 'auto',
                    marginLeft: 'auto',
                    maxWidth: '100%',
                },
                style,
            ]}
        />
    ) : null;
};
export default Image;
