import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View} from 'react-native';
import {useSelector} from 'react-redux';

/**
 *
 * @typedef Props
 * @property {Boolean} outline
 * @property {String} color
 * @property {import('react-native').ViewStyle} style
 */
const Card = ({style, children, color = 'container', outline = false}) => {
    const {styles, colors} = useTheme();
    const isDarkMode = useSelector(s => s.theme?.isDarkMode ?? false);

    const _color = colors[color] || color;
    return (
        <View
            style={[
                styles.card,
                style,
                isDarkMode && outline
                    ? {
                          borderColor: _color,
                          borderWidth: 1,
                          backgroundColor: colors.container,
                      }
                    : {backgroundColor: _color},
            ]}>
            <>{children}</>
        </View>
    );
};

export default Card;
