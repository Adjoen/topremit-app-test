export default {
    primary: '#006E7F',
    primaryDark: '#00515e',
    primaryLight: '#0094aa',

    black: '#1b1c1e',
    black2: '#1f1f1f',
    black3: '#2b2b2b',
    black4: '#303030',
    black5: '#3f3f3f',
    silver: '#7f8c8d',
    silver2: '#636e72',

    grey: '#4F5B66',

    white: '#ffffff',
    white1: '#f3f3f3',
    white2: '#F3F7FA',
    white3: '#bbbbbb',
    white4: '#aaaaaa',
    white5: '#888888',
    white6: '#444444',

    success: '#1BA345',
    warning: '#FEC001',
    danger: '#dc3545',
};
