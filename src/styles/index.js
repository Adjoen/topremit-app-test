import {StyleSheet} from 'react-native';
import layout from './layout';
import margin from './margin';
import padding from './padding';
import border from './border';
import colors from './colors';

export const COLOR = colors;
export const FONT = {
    bold: 'OpenSans-Bold',
    boldItalic: 'OpenSans-BoldItalic',
    extraBold: 'OpenSans-ExtraBold',
    extraBoldItalic: 'OpenSans-ExtraBoldItalic',
    italic: 'OpenSans-Italic',
    light: 'OpenSans-Light',
    lightItalic: 'OpenSans-LightItalic',
    medium: 'OpenSans-Medium',
    mediumItalic: 'OpenSans-MediumItalic',
    regular: 'OpenSans-Regular',
    semiBold: 'OpenSans-SemiBold',
    semiBoldItalic: 'OpenSans-SemiBoldItalic',
};

export const FONT_SIZE = {
    xs: 12,
    sm: 14,
    md: 16,
    lg: 18,
    xl: 20,
    '2xl': 22,
    '3xl': 24,
    '4xl': 26,
    '5xl': 28,
};

const styles = StyleSheet.create({
    bgPrimary: {backgroundColor: COLOR.primary},
    bgPrimaryDarker: {backgroundColor: COLOR.primaryDarker},
    bgPrimaryLight: {backgroundColor: COLOR.primaryLight},
    bgSecondary: {backgroundColor: COLOR.secondary},
    bgSecondaryLight: {backgroundColor: COLOR.secondaryLight},
    bgSecondaryDarker: {backgroundColor: COLOR.secondaryDarker},
    bgTertiary: {backgroundColor: COLOR.tertiary},
    bgWhite: {backgroundColor: COLOR.white},
    bgWhite1: {backgroundColor: COLOR.white1},
    bgWhite2: {backgroundColor: COLOR.white2},
    bgWhite3: {backgroundColor: COLOR.white3},
    bgWhite4: {backgroundColor: COLOR.white4},
    bgWhite5: {backgroundColor: COLOR.white5},
    bgBlack: {backgroundColor: COLOR.black},
    bgBlack2: {backgroundColor: COLOR.black2},
    bgBlack3: {backgroundColor: COLOR.black3},
    bgBlack4: {backgroundColor: COLOR.black4},
    bgBlack5: {backgroundColor: COLOR.black5},

    bgOverlay: {backgroundColor: 'rgba(0, 0, 0, 0.6)'},

    bgGrey: {backgroundColor: COLOR.grey},

    bgDanger: {backgroundColor: COLOR.danger},
    bgSuccess: {backgroundColor: COLOR.success},
    bgWarning: {backgroundColor: COLOR.warning},

    container: {
        flex: 1,
        backgroundColor: COLOR.white1,
    },

    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },

    boxShadow: {
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
    },

    boxShadowSmooth: {
        shadowColor: '#AAAAAA',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 1,
    },

    overflowVisible: {
        overflow: 'visible',
    },

    overflowHidden: {
        overflow: 'hidden',
    },

    smoothBorder: {
        borderRadius: 8,
    },

    borderM: size => {
        return {borderRadius: size};
    },
});

const _ = {
    ...margin,
    ...padding,
    ...border,
    ...layout,
    ...styles,
};
export default _;
