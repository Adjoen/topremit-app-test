export default {
    sm: 4,
    base: 16,
    md: 8,
    lg: 20,
    xl: 24,
    '2xl': 28,
    '3xl': 32,
    full: 999,
};
