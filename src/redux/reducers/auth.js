import {AUTH_LOADING_PASS} from '../type';

const initialState = {
  loading: true,
};
const authReducers = (state = initialState, {value, type}) => {
  switch (type) {
    case AUTH_LOADING_PASS:
      return {...state, ...{loading: false}};
    default:
      return state;
  }
};

export default authReducers;
