import {THEME_PATCH} from '../type';

const initialState = {
  isDarkMode: false,
};
const themeReducers = (state = initialState, {value, type}) => {
  switch (type) {
    case THEME_PATCH:
      return {...state, ...value};
    default:
      return state;
  }
};

export default themeReducers;
