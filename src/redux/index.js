import {combineReducers, createStore} from '@reduxjs/toolkit';
import authReducers from './reducers/auth';
import themeReducers from './reducers/theme';

const rootReducer = combineReducers({
  auth: authReducers,
  theme: themeReducers,
});
export const store = createStore(rootReducer);
