import {AUTH_LOADING_PASS} from '../type';

export const passLoading = () => {
  return {
    type: AUTH_LOADING_PASS,
  };
};
