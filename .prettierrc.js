module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  trailingComma: 'es5',
  singleQuote: true,
  tabWidth: 4,
  printWidth: 150,
  arrowParens: 'avoid',
};
